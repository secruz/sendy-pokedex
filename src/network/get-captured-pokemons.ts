import {POKEMON_API, RESOURCE_POKEMONS_CAPTURED} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';
import {PokemonModel} from './shared';

interface CapturedPokemon {
  readonly id: number;
  readonly pokemon: PokemonModel;
}

interface CapturedPokemonsResponse {
  readonly capturedPokemons: CapturedPokemon[];
}

export async function getCapturedPokemons(): Promise<CapturedPokemonsResponse> {
  const URL = `${POKEMON_API}${RESOURCE_POKEMONS_CAPTURED}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const normalizedResponse = normalizeCapturedPokemons(await response.json());

    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: encounter was not found`);
  }
}

function normalizeCapturedPokemons({
  capturedPokemons,
}): CapturedPokemonsResponse {
  return {
    capturedPokemons: Array.isArray(capturedPokemons)
      ? capturedPokemons.map(
          ({id: capturedID, pokemon: {id, name, weight, height}}) => {
            return {
              id: capturedID,
              pokemon: {
                id,
                name,
                weight,
                height,
              },
            };
          },
        )
      : [],
  };
}
