import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import I18n from './src/I18n/locales/index';

export class Languages extends React.Component {
  render() {
    return (
      <View>
        <Text style={styles.text}>{I18n.t('title_pokedex')}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontWeight: 'bold',
    marginVertical: 2,
    justifyContent: 'center',
  },
});
