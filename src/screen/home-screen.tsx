import {Button, Text, View} from 'react-native';
import {Languages} from '../../languages';
import * as React from 'react';
import {FeatureItem} from '../components/feature-item';
import {FeaturePanel} from '../components/feature-panel';

interface HomeScreenProps {
  readonly navigation: any;
}

export function HomeScreen(props: HomeScreenProps) {
  return (
    <View
      style={{
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'gray',
      }}>
      <Text>Home Screen</Text>
      <View style={{margin: 10}}>
        <Button
          title="Endpoints"
          onPress={() => props.navigation.navigate('Endpoints')}
        />
      </View>
      <View style={{margin: 10}}>
        <Button
          title="Init App"
          onPress={() => props.navigation.navigate('Pokemon Welcome')}
        />
      </View>
      <Languages />
      <View
        style={{
          margin: 10,
          backgroundColor: 'red',
          width: '100%',
        }}>
        <FeatureItem featureName={'Altura'} featureValue={'1.1m'} />
      </View>
      <View
        style={{
          width: '60%',
          margin: 10,
          flex: 1,
          alignContent: 'flex-end',
        }}>
        <FeaturePanel
          features={[
            {
              name: 'Altura',
              value: '1.1 m',
            },
            {name: 'Peso', value: '19,0 Kg'},
            {name: 'Habitat', value: 'Montaña'},
            {
              name:
                'Altura del pokemon visible Altura del pokemon visible Altura del pokemon visible',
              value: '1.1 m 1.1 m 1.1 m 1.1 m 1.1 m 1.1 m 1.1 m',
            },
          ]}
        />
      </View>
    </View>
  );
}
