import {FlatList, StyleSheet, Text, View} from 'react-native';
import * as React from 'react';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
}

const generations = ['1', '2', '3', '4', '5', '6', '7'];

export class PokemonGenerationsScreen extends React.Component<Props> {
  public constructor(props: Props) {
    super(props);
  }

  public render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View style={[{margin: 10}]}>
          <PokemonLogo />

          <FlatList
            data={generations}
            renderItem={({item}) => {
              return (
                <View style={styles.container}>
                  <Text
                    style={styles.text}
                    onPress={() =>
                      this.props.navigation.navigate('Generation Screen', {
                        generation: item,
                      })
                    }>
                    {item}
                  </Text>
                </View>
              );
            }}
            keyExtractor={item => {
              return item;
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 50,
    fontWeight: 'bold',
    alignSelf: 'center',
    fontFamily: 'i',
    color: '#2e10a7',
  },
  container: {
    flex: 1,
    margin: 10,
    backgroundColor: '#f2cb43',
    borderRadius: 30,
  },
});
