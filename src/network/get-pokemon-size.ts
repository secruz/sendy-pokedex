import {BASE_URL, POKEMON_NAME_PARAM, RESOURCE_POKEMON} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonSizeResponse {
  readonly height: string;
  readonly weight: string;
}

export async function getPokemonSize(
  name: string,
): Promise<PokemonSizeResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const jsonResponse = await response.json();
    const normalizedResponse = normalizePokemonSize(jsonResponse);

    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: Weigth or Height was not found for id: ${name}`);
  }
}

function normalizePokemonSize({height, weight}): PokemonSizeResponse {
  return {
    height,
    weight,
  };
}
