import {POKEMON_API, RESOURCE_POKEMON_CAPTURE} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonCaptureResponse {
  readonly captured: boolean;
  readonly pokemon?: PokemonModel;
  readonly remainingAttempts: number;
}

interface PokemonModel {
  readonly id: number;
  readonly name: string;
  readonly weight: number;
  readonly height: number;
}

export async function capturePokemon(
  encounterToken: string,
): Promise<PokemonCaptureResponse> {
  const URL = `${POKEMON_API}${RESOURCE_POKEMON_CAPTURE}/${encounterToken}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    const response2 = await response.json();
    // normalize
    const normalizedResponse = normalizePokemonCapture(response2);

    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: capture was not found ${encounterToken}`);
  }
}

function normalizePokemonCapture({
  captured,
  pokemon,
  remainingAttempts,
}): PokemonCaptureResponse {
  return {
    captured,
    pokemon: pokemon
      ? {
          id: pokemon.id,
          name: pokemon.name,
          height: pokemon.height,
          weight: pokemon.weight,
        }
      : undefined,
    remainingAttempts,
  };
}
