import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {FeatureItem} from '../feature-item';

export interface Feature {
  readonly name: string;
  readonly value: string;
}

interface FeaturePanelProps {
  readonly features: Feature[];
}

export function FeaturePanel({features}: FeaturePanelProps) {
  return (
    <View style={styles.rootContainer}>
      {features.map(({name, value}, index) => {
        return (
          <FeatureItem key={index} featureName={name} featureValue={value} />
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: 'column',
    backgroundColor: '#dd8b2a',
    borderRadius: 10,
    padding: 10,
    flex: 1,
  },
});
