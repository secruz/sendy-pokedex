import {Image, ScrollView, Text, View, StyleSheet} from 'react-native';
import * as React from 'react';
import {getPokemonEvolution} from '../network/get-pokemon-evolution';
import {getPokemonDescription} from '../network/get-pokemon-description';
import {getPokemonSize} from '../network/get-pokemon-size';
import {getPokemonImage} from '../network/get-pokemon-image';
import {FeaturePanel} from '../components/feature-panel';
import {Feature} from '../components/feature-panel/feature-panel';
import {getPokemonTypes} from '../network/get-pokemon-type';
import {getPokemonHabitat} from '../network/get-pokemon-habitat';
import {getPokemonAbilities} from '../network/get-pokemon-abilities';
import {getPokemonShape} from '../network/get-pokemon-shape';
import I18n from '../I18n/locales/index';
import {PokemonButton} from '../components/pokemon-button';
import {getPokemonEvolutionID} from '../network/get-pokemon-evolution-id';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

interface PokemonInfo {
  readonly description: string;
  readonly features: Feature[];
  readonly imgURL: string;
  readonly name: string;
}

interface State {
  readonly isLoading: boolean;
  readonly pokemonInfo?: PokemonInfo;
}

export class PokemonEvolutionScreen extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  private pokemonName: string = '';

  public async componentDidMount() {
    this.pokemonName = this.props.route.params?.pokemonName;

    const {evolutionID} = await getPokemonEvolutionID(this.pokemonName);

    const {evolutionName} = await getPokemonEvolution(evolutionID);
    console.log(`evolutionName: ${evolutionName}`);

    const {description} = await getPokemonDescription(evolutionName);
    console.log(`description: ${description}`);

    const {height} = await getPokemonSize(evolutionName);
    console.log(`height: ${height}`);

    const {weight} = await getPokemonSize(evolutionName);
    console.log(`weight: ${weight}`);

    const {shape} = await getPokemonShape(evolutionName);
    console.log(`shape: ${shape}`);

    const {types} = await getPokemonTypes(evolutionName);
    console.log(`types: ${types}`);

    const {habitat} = await getPokemonHabitat(evolutionName);
    console.log(`habitat: ${habitat}`);

    const {abilities} = await getPokemonAbilities(evolutionName);
    console.log(`abilities: ${abilities}`);

    const {imgUrl} = await getPokemonImage(evolutionName);
    console.log(`imgUrl: ${imgUrl}`);

    /* init state with response data */

    // request finished
    this.setState({
      isLoading: false,
      pokemonInfo: {
        name: evolutionName,
        description,
        features: [
          {
            name: I18n.t('title_height'),
            value: height,
          },
          {
            name: I18n.t('title_weight'),
            value: weight,
          },
          {
            name: I18n.t('title_shape'),
            value: shape,
          },
          {
            name: I18n.t('title_habitat'),
            value: habitat,
          },
          {
            name: I18n.t('title_abilities'),
            value: abilities.toString(),
          },
          {
            name: I18n.t('title_types'),
            value: types.toString(),
          },
        ],
        imgURL: imgUrl,
      },
    });
  }

  public render() {
    const {navigation} = this.props;
    const {isLoading, pokemonInfo} = this.state;

    if (isLoading) {
      return <Text> is loading ...</Text>;
    }

    if (!pokemonInfo) {
      return <Text> Pokemon info is not available.</Text>;
    }

    const {name, description, features, imgURL} = pokemonInfo;

    return (
      <ScrollView>
        <View>
          <View style={[{alignItems: 'center', flex: 1}]}>
            <PokemonLogo />
          </View>

          <Text style={styles.title}>{name}</Text>
          <Text style={styles.description}>{description}</Text>

          <View
            style={{margin: 15, flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={{
                width: 100,
                height: 100,
              }}
              source={{uri: imgURL}}
            />
            <FeaturePanel features={features} />
          </View>

          <View style={[{margin: 10, flexDirection: 'row'}]}>
            <PokemonButton
              showBorder={false}
              title={I18n.t('title_return')}
              onPress={() => navigation.navigate('Pokemon Details')}
            />
            <View style={{width: 10}}></View>

            <PokemonButton
              showBorder={false}
              isDarkMode={false}
              title={I18n.t('action_pokemonSearch')}
              onPress={() => navigation.navigate('PokemonSearch')}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  description: {
    fontSize: 15,
    textAlign: 'justify',
    alignSelf: 'center',
  },
  btnTextSearch: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  btnTextReturn: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#FFFFFF',
  },
  btnSearch: {
    fontSize: 10,
    fontWeight: 'bold',
    height: 50,
    width: 180,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#f2cb43',
    padding: 6,
  },
  btnReturn: {
    fontSize: 10,
    fontWeight: 'bold',
    height: 50,
    width: 180,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#2e10a7',
    padding: 6,
  },
});
