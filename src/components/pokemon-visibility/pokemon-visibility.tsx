import {Image} from 'react-native';
import * as React from 'react';

interface Props {
  readonly imgURL: string;
  readonly captureAttempts: number;
}

const imgSize = 300;

export function PokemonVisibility({imgURL, captureAttempts}: Props) {
  return (
    <Image
      style={{
        width: imgSize,
        height: imgSize,
        opacity: getOpacityByCaptureAttempts(captureAttempts),
      }}
      source={{uri: imgURL}}
    />
  );
}

function getOpacityByCaptureAttempts(attemps: number): number {
  switch (attemps) {
    case 2:
      return 1;
    case 1:
      return 0.7;
    case 0:
      return 0.3;
    default:
      return 1;
  }
}
