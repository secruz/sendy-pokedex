import {POKEMON_API, RESOURCE_POKEMON_ENCOUNTER} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';
import {PokemonModel} from './shared';

interface PokemonEncounterResponse {
  readonly encounter: boolean;
  readonly encounterToken: string;
  readonly pokemon?: PokemonModel;
}

export async function encounterPokemonRequest(): Promise<
  PokemonEncounterResponse
> {
  const URL = `${POKEMON_API}${RESOURCE_POKEMON_ENCOUNTER}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success

    // normalize
    const normalizedResponse = normalizePokemonEncounter(await response.json());
    // return pokemon encounter
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: encounter was not found`);
  }
}

function normalizePokemonEncounter({
  encounter,
  encounterToken,
  pokemon,
}): PokemonEncounterResponse {
  return {
    encounter,
    encounterToken,
    pokemon: pokemon
      ? {
          id: pokemon.id,
          name: pokemon.name,
          height: pokemon.height,
          weight: pokemon.weight,
        }
      : undefined,
  };
}
