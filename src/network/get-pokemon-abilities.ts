import {BASE_URL, POKEMON_NAME_PARAM, RESOURCE_POKEMON} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonAbilitiesResponse {
  readonly abilities: string[];
}

export async function getPokemonAbilities(
  name: string,
): Promise<PokemonAbilitiesResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;
  if (status === HTTP_RESPONSE_OK) {
    const jsonResponse = await response.json();

    const normalizedResponse = normalizePokemonAbilities(jsonResponse);
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: Abilities was not found for id: ${name}`);
  }
}

function normalizePokemonAbilities({abilities}): PokemonAbilitiesResponse {
  const abilitiesNames: string[] = [];
  for (const {ability} of abilities) {
    abilitiesNames.push(ability.name);
  }
  return {
    abilities: abilitiesNames,
  };
}
