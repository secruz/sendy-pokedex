import {
  View,
  ImageBackground,
  ScrollView,
  Text,
  Alert,
  StyleSheet,
} from 'react-native';
import * as React from 'react';
import {PokemonButton} from '../components/pokemon-button';
import {AdventureLoadingIndicator} from '../components/adventure-loading-indicator';
import {PokemonLogo} from '../components/pokemon-logo';
import {encounterPokemonRequest} from '../network/encounter-pokemon-request';
import {capturePokemon} from '../network/capture-pokemon';
import {getPokemonImage} from '../network/get-pokemon-image';
import {PokemonPokebola} from '../components/pokemon-pokebola';
import {PokemonVisibility} from '../components/pokemon-visibility';
import I18n from '../I18n/locales/index';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

interface PokemonInfo {
  readonly name: string;
  readonly imgUrl: string;
  readonly token: string;
}

interface State {
  readonly isLoading: boolean;
  readonly pokemonInfo?: PokemonInfo;
  readonly allowCaptured: boolean;
  readonly allowEncounter: boolean;
  readonly captureAttempts: number;
}

export class PokemonAdventureScreen extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: false,
      allowCaptured: true,
      allowEncounter: true,
      captureAttempts: 2,
    };
  }

  private encounterPokemon = async () => {
    this.setState({
      isLoading: true,
      allowCaptured: false,
      allowEncounter: true,
    });

    const {
      encounterToken,
      encounter,
      pokemon,
    } = await encounterPokemonRequest();

    if (encounter) {
      const {name: pokemonName} = pokemon;
      const {imgUrl} = await getPokemonImage(pokemonName);

      this.setState({
        pokemonInfo: {
          name: pokemonName,
          imgUrl,
          token: encounterToken,
        },
        isLoading: false,
        allowEncounter: false,
        allowCaptured: false,
      });
    } else {
      Alert.alert(I18n.t('detail_pokemonNotEncountered'));
      this.setState({
        pokemonInfo: undefined,
        isLoading: false,
        allowEncounter: true,
        allowCaptured: true,
      });
    }
  };

  private attemptToCapturePokemon = async () => {
    const {navigation} = this.props;
    if (this.state.pokemonInfo) {
      const {token} = this.state.pokemonInfo;

      const {remainingAttempts, captured} = await capturePokemon(token);

      if (remainingAttempts === 0) {
        this.setState({
          captureAttempts: remainingAttempts,
        });
        this.setState({
          pokemonInfo: undefined,
          isLoading: false,
          allowEncounter: true,
          allowCaptured: true,
          captureAttempts: 2,
        });
        if (captured) {
          Alert.alert(
            I18n.t('title_pokemonCaptured'),
            I18n.t('title_capturedPokemon'),
            [
              {
                text: I18n.t('action_seeCapturedPokemons'),
                onPress: () => navigation.navigate('Captured Pokemons'),
              },
              {
                text: 'OK',
              },
            ],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            I18n.t('title_pokemonNotCaptured'),
            I18n.t('detail_pokemonNotCaptured'),
            [
              {
                text: 'OK',
                onPress: () => navigation.navigate('Pokemon Adventure'),
              },
            ],
          );
        }
      } else {
        this.setState({
          captureAttempts: remainingAttempts,
        });
        Alert.alert(
          I18n.t('title_pokemonNotCaptured'),
          I18n.t('detail_pokemonNotCaptured'),
          [
            {
              text: 'OK',
            },
          ],
        );
      }
    }
  };

  public render() {
    const {
      allowCaptured,
      isLoading,
      allowEncounter,
      pokemonInfo,
      captureAttempts,
    } = this.state;

    return (
      <ScrollView>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <PokemonLogo />
          <View>
            <ImageBackground
              source={require('../resources/adventure-background.png')}
              style={{width: 600, height: 550}}>
              {pokemonInfo && (
                <View style={{alignItems: 'center', marginTop: 70}}>
                  <Text style={{fontSize: 60}}>
                    {pokemonInfo.name.toUpperCase()}
                  </Text>
                  <View style={{marginTop: 10}}>
                    <Text style={{fontSize: 25}}>
                      {I18n.t('title_captureAttempts')} {captureAttempts}
                    </Text>
                  </View>
                  <PokemonVisibility
                    imgURL={pokemonInfo.imgUrl}
                    captureAttempts={captureAttempts}
                  />

                  <View style={{position: 'absolute', marginTop: 370}}>
                    <PokemonPokebola
                      onPress={() => {
                        this.attemptToCapturePokemon();
                      }}
                    />
                  </View>
                </View>
              )}
              <AdventureLoadingIndicator
                width={300}
                height={400}
                isLoading={isLoading}
              />
            </ImageBackground>
          </View>

          {allowEncounter && (
            <View style={[{margin: 10, flexDirection: 'row'}]}>
              <PokemonButton
                showBorder={true}
                isDarkMode={false}
                title={I18n.t('action_encounter').toUpperCase()}
                onPress={this.encounterPokemon}
              />
            </View>
          )}

          {allowCaptured && (
            <View style={[{margin: 10, flexDirection: 'row'}]}>
              <PokemonButton
                showBorder={true}
                isDarkMode={false}
                title={I18n.t('action_seeCapturedPokemons').toUpperCase()}
                onPress={() =>
                  this.props.navigation.navigate('Captured Pokemons')
                }
              />
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    margin: 370,
  },
});
