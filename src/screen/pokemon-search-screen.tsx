import {FlatList, StyleSheet, Text, View} from 'react-native';
import * as React from 'react';
import {getPokemonList} from '../network/get-pokemon-list';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
}

interface State {
  readonly isLoading: boolean;
  readonly pokemons: string[];
}

export class PokemonSearchScreen extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: true,
      pokemons: [],
    };
  }

  public async componentDidMount() {
    const {pokemonEntries} = await getPokemonList();
    console.log(`pokemonEntries: ${pokemonEntries}`);

    /* consume other endpoints to retrieve pokemon data in the screen */
    /* init state with response data */

    // request finished
    this.setState({
      isLoading: false,
      pokemons: pokemonEntries,
    });
  }

  public render() {
    const {isLoading, pokemons} = this.state;

    if (isLoading) {
      return <Text> is loading ...</Text>;
    }

    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View style={[{margin: 10}]}>
          <PokemonLogo />

          <FlatList
            data={pokemons}
            renderItem={({item}) => {
              return (
                <View style={styles.container}>
                  <Text
                    style={styles.text}
                    onPress={() =>
                      this.props.navigation.navigate('Pokemon Details', {
                        pokemonName: item,
                      })
                    }>
                    {item}
                  </Text>
                </View>
              );
            }}
            keyExtractor={item => {
              return item;
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 50,
    fontWeight: 'bold',
    alignSelf: 'center',
    fontFamily: 'i',
    color: '#2e10a7',
  },
  container: {
    flex: 1,
    margin: 10,
    backgroundColor: '#f2cb43',
    borderRadius: 30,
  },
});
