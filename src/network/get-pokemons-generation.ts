import {
  BASE_URL,
  GENERATION_ID_PARAM,
  RESOURCE_POKEMONS_GENERATION,
} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

export interface PokemonsByGeneration {
  readonly pokemonNames: string;
}

interface PokemonGenerationResponse {
  readonly pokemonSpecies: PokemonsByGeneration[];
}

export async function getPokemonGeneration(
  id: string,
): Promise<PokemonGenerationResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMONS_GENERATION.replace(
    GENERATION_ID_PARAM,
    id,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;
  if (status === HTTP_RESPONSE_OK) {
    const jsonResponse = await response.json();

    const normalizedResponse = normalizePokemonGeneration(jsonResponse);
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: Generation was not found for id: ${id}`);
  }
}

function normalizePokemonGeneration({
  pokemon_species,
}): PokemonGenerationResponse {
  return {
    pokemonSpecies: Array.isArray(pokemon_species)
      ? pokemon_species.map(({name}) => {
          return {
            pokemonNames: name,
          };
        })
      : [],
  };
}
