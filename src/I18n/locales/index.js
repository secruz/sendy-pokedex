import I18n from 'react-native-i18n';
import es from './es';
import en from './en';

I18n.fallbacks = true;
I18n.defaultLocale = 'es';

I18n.translations = {
  en,
  es,
};
export default I18n;
