import {BASE_URL, POKEMON_NAME_PARAM, RESOURCE_POKEMON} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonImageResponse {
  readonly imgUrl: string;
}

export async function getPokemonImage(
  name: string,
): Promise<PokemonImageResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    return normalizePokemonImage(await response.json());
  } else {
    // fail

    console.log(`Error: image was not found for id: ${name}`);
    return normalizePokemonImage({});
  }
}

function normalizePokemonImage(pokemon): PokemonImageResponse {
  return {
    imgUrl:
      pokemon.sprites && pokemon.sprites.front_default
        ? pokemon.sprites.front_default
        : 'https://reactnative.dev/img/tiny_logo.png',
  };
}
