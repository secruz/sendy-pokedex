import React from 'react';
import {View, StyleSheet, ScrollView, Image} from 'react-native';
import I18n from '../I18n/locales/index';
import {PokemonButton} from '../components/pokemon-button';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

export function PokemonMenuSearchScreen(props: Props) {
  return (
    <ScrollView>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <PokemonLogo />
        <View style={[{margin: 40}]}>
          <Image
            source={require('../resources/pokemon-search.png')}
            style={{width: 400, height: 200}}
          />
        </View>
        <View style={[{margin: 10, flexDirection: 'row'}]}>
          <PokemonButton
            showBorder={true}
            isDarkMode={false}
            title={I18n.t('action_goToSearchPokemons').toUpperCase()}
            onPress={() => props.navigation.navigate('PokemonSearch')}
          />
        </View>

        <View style={[{margin: 10, flexDirection: 'row'}]}>
          <PokemonButton
            showBorder={true}
            isDarkMode={false}
            title={I18n.t('action_goToPokemonsByGeneration').toUpperCase()}
            onPress={() => props.navigation.navigate('Generations')}
          />
        </View>
      </View>
    </ScrollView>
  );
}
