# Pokedex

### Installation

Project uses **yarn**

- Install react native CLI

  `yarn global add react-native-cli`

- Install Dependencies

  `yarn install`

- Link Resources

  `yarn run link` [more info here](https://blog.bam.tech/developper-news/add-a-custom-font-to-your-react-native-app)

- for Android, verify **ANDROID_HOME** and **ADB** are properly referenced

### Run

- connect a physical device or start an emulator

##### Android

- `yarn run android`

##### Validate prettier format

Requirements:

- IDEA 2020.2.1
- Prettier Plugin

Use the “Reformat with Prettier” action (**Alt-Shift-Cmd-P** on macOS or **Alt-Shift-Ctrl-P** on Windows and Linux) or find it using the “Find Action” popup (Cmd/Ctrl-Shift-A)

### Emulator

##### Android

- run adb emulator from terminal:

  - list emulators `emulator -list-avds`

  - start emulator `emulator -avd {emulator_name}`, e.g. `emulator -avd nexus_5x_api_22`

### Code Format

You can format the code by using Prettier, these are the steps:

###### Intellij IDEA

- Install "Prettier" plugin (File -> Settings -> Plugins -> Search for "Prettier")
- Install "prettier" dependency: `yarn add prettier`
- Use the “Reformat with Prettier” action (Alt-Shift-Cmd-P on macOS or Alt-Shift-Ctrl-P on Windows and Linux) or find it using the “Find Action” popup (Cmd/Ctrl-Shift-A)

## Run API using heroku

To successfully run the encounter endpoint, it is necessary to consume heroku from the next server:
https://pokemon-alebrije.herokuapp.com/api/pokemon/encounter

Access data:
email: ariosm.developer@gmail.com
pwd: entrenamiento123-

# Run API using localhost mode

To successfully execute the consumption of the encounter endpoint, it is necessary to modify the URL found in line 23 of get-pokemon-encounter.ts
"http://10.0.0.9:8080/api/pokemon/encounter"
With the address of the computer where it is running. In the current URL the IP is 10.0.0.9.
To find out the computer's IP, run the "ipconfig" command on the console.
