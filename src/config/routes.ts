// base url
export const BASE_URL = 'https://pokeapi.co/api/v2/';
export const POKEMON_API =
  'https://pokemon-alebrije.herokuapp.com/api/pokemon/';

// param
export const POKEMON_NAME_PARAM = '{POKEMON_NAME}';
export const EVOLUTION_ID_PARAM = '{EVOLUTION_ID}';
export const GENERATION_ID_PARAM = '{GENERATION_ID}';

// routes
export const RESOURCE_POKEMON_SPECIES = 'pokemon-species/';
export const RESOURCE_POKEMON_SPECIES_BY_NAME = `pokemon-species/${POKEMON_NAME_PARAM}`;
export const RESOURCE_POKEMON_LIST = 'pokedex/1/'; // where "1" is the default pokedex
export const RESOURCE_POKEMON = `pokemon/${POKEMON_NAME_PARAM}`;
export const RESOURCE_POKEMON_EVOLUTION = `evolution-chain/${EVOLUTION_ID_PARAM}`;
export const RESOURCE_POKEMON_ENCOUNTER = 'encounter';
export const RESOURCE_POKEMON_CAPTURE = `${RESOURCE_POKEMON_ENCOUNTER}/capture`;
export const RESOURCE_POKEMONS_CAPTURED = `captured`;
export const RESOURCE_POKEMONS_GENERATION = `generation/${GENERATION_ID_PARAM}`;
