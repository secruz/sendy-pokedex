import {BASE_URL, POKEMON_NAME_PARAM, RESOURCE_POKEMON} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonTypesResponse {
  readonly types: string[];
}

export async function getPokemonTypes(
  name: string,
): Promise<PokemonTypesResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;
  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const jsonResponse = await response.json();

    const normalizedResponse = normalizePokemonTypes(jsonResponse);
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: Types was not found for id: ${name}`);
  }
}

function normalizePokemonTypes({types}): PokemonTypesResponse {
  return {
    types: types.map(item => item.type.name),
  };
}
