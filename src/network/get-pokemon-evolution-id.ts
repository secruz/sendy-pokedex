import {
  BASE_URL,
  RESOURCE_POKEMON_SPECIES_BY_NAME,
  POKEMON_NAME_PARAM,
} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonEvolutionIDResponse {
  readonly evolutionID: string;
}

export async function getPokemonEvolutionID(
  name: string,
): Promise<PokemonEvolutionIDResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_SPECIES_BY_NAME.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const jsonResponse = await response.json();
    const normalizedResponse = normalizePokemonEvolution(jsonResponse);

    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: name of evolution was not found for id: ${name}`);
  }
}

function normalizePokemonEvolution({
  evolution_chain: {url},
}): PokemonEvolutionIDResponse {
  const tokens = url.split('evolution-chain');
  const tokenID = tokens[1];
  const idTokens = tokenID.split('/');

  const evolutionID = idTokens[1];
  return {
    evolutionID,
  };
}
