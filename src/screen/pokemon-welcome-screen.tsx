import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Alert,
  ScrollView,
  Image,
} from 'react-native';
import I18n from '../I18n/locales/index';
import es from '../I18n/locales/es';
import en from '../I18n/locales/en';
import {PokemonButton} from '../components/pokemon-button';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

interface State {
  readonly changeLanguage: string;
}

export class PokemonWelcomeScreen extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      changeLanguage: 'en',
    };
  }

  render() {
    const {navigation} = this.props;
    return (
      <ScrollView>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <PokemonLogo />
          <View style={[{margin: 40}]}>
            <Image
              source={require('../resources/pokemons.png')}
              style={{width: 400, height: 200}}
            />
          </View>
          <View style={[{margin: 10, flexDirection: 'row'}]}>
            <PokemonButton
              showBorder={true}
              isDarkMode={false}
              title={'POKEDEX'}
              onPress={() => navigation.navigate('Menu Search')}
            />
          </View>

          <View style={[{margin: 10, flexDirection: 'row'}]}>
            <PokemonButton
              showBorder={true}
              isDarkMode={false}
              title={I18n.t('action_goToAdventureSection').toUpperCase()}
              onPress={() => navigation.navigate('Pokemon Adventure')}
            />
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              Alert.alert(
                I18n.t('title_languageSelection'),
                I18n.t('title_translate'),
                [
                  {
                    text: I18n.t('title_textSpanish'),
                    onPress: () => {
                      I18n.locale = 'sp-Us';
                      this.setState({changeLanguage: 'es'});
                    },
                  },
                  {
                    text: I18n.t('title_textEnglish'),
                    onPress: () => {
                      I18n.locale = 'en-Us';
                      this.setState({changeLanguage: 'en'});
                    },
                  },

                  {
                    text: I18n.t('title_textCancel'),
                  },
                ],
                {cancelable: false},
              );
            }}>
            <Text>{I18n.t('action_clickChangeLanguage')}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#F5FCFF', flex: 1, alignItems: 'center'},
  text: {
    paddingVertical: 5,
  },
  button: {
    alignSelf: 'center',
    marginVertical: 10,
    borderRadius: 10,
    paddingHorizontal: 10,
  },
  mainTitle: {
    flex: 1,
    justifyContent: 'center',
  },
});
